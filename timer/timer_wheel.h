#ifndef TIME_WHEEL_TIMER
#define TIME_WHEEL_TIMER

#include <netinet/in.h>
#include <stdio.h>
#include <time.h>

#define BUFFER_SIZE 64

class tw_timer;
/* 绑定 socket 和定时器 */
struct client_data {
  sockaddr_in address;
  int sockfd;
  char buf[BUFFER_SIZE];
  tw_timer *timer;
};

// 定时器类
class tw_timer {
 public:
  tw_timer(int rot, int ts)
      : next(NULL), prev(NULL), rotation(rot), time_slot(ts) {}

 public:
  // 记录定时器在时间轮中转多少圈后生效
  int rotation;
  // 记录定时器属于时间轮上哪个槽(对应链表，下同);
  int time_slot;
  // 定时器回调函数
  void (*cb_func)(client_data);
  // 用户数据
  client_data *user_data;
  // 指向下一个定时器
  tw_timer *next;
  // 指向上一个定时器
  tw_timer *prev;
};

#endif  // TIME_WHEEL_TIMER
