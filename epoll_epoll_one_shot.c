#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_EVENT_NUMBER 1024
#define BUFFER_SIZE 1024

struct fds {
  int epollfd;
  int sockfd;
};

void setnonblocking(int fd) {
  int old_option = fcntl(fd, F_GETFL);
  int new_option = old_option | O_NONBLOCK;
  fcntl(fd, F_SETFL, new_option);
  return old_option;
}

/* 将 fd 上的 EPOLLIN 和 EPOLLET 事件注册到 epollfd 指示的 epoll
 * 内核事件表中，参数 oneshot 表示是否注册 fd 上的 EPOLLONESHOT 事件 */
void add_fd(int epollfd, int fd, int oneshot) {
  struct epoll_event event;
  event.data.fd = fd;
  event.events = EPOLLIN | EPOLLET;
  if (oneshot) {
    event.events |= EPOLLONESHOT;
  }
  epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &event);
  setnonblocking(fd);
}

/* 重置 fd 上的事件。这样操作之后，尽管 fd 上的 EPOLLONESHOT
 * 事件被注册。但是操作系统仍然会触发 fd 上的 EPOLLIN 事件，且只触发一次。 */
void reset_oneshot(int epollfd, int fd) {
  struct epoll_event event;
  event.data.fd = fd;
  event.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
  epoll_ctl(epollfd, EPOLL_CTL_MOD, fd, &event);
}

/* 工作线程 */
void *worker(void *arg) {
  int sockfd = ((struct fds *)arg)->sockfd;
  int epollfd = ((struct fds *)arg)->epollfd;
  printf("start new thread to receive data on fd: %d\n", sockfd);
  char buf[BUFFER_SIZE];
  memset(buf, 0x0, BUFFER_SIZE);
  /* 循环读取 sockfd 上的数据，直到遇到	EAGAIN 错误 */
  while (1) {
    int ret = recv(sockfd, buf, BUFFER_SIZE - 1, 0);
    if (ret == 0) {
      close(sockfd);
      printf("foreiner closed the connection\n");
      break;
    } else if (ret < 0) {
      if (errno == EAGAIN) {
        reset_oneshot(epollfd, sockfd);
        printf("read later\n");
        break;
      }
    } else {
      printf("get content: %s\n", buf);
      /* 休眠 5s，模拟数据处理 */
      sleep(5);
    }
  }
  printf("end thread receiving data on fd: %d\n", sockfd);
}

int main(int argc, char *argv[]) {
  const char *ip = argv[1];
  int32_t port = atoi(argv[2]);

  int ret = 0;
  struct sockaddr_in address;
  bzero(&address, sizeof(address));
  address.sin_family = AF_INET;
  inet_pton(AF_INET, ip, &address.sin_addr);
  address.sin_port = htons(port);

  int listenfd = socket(PF_INET, SOCK_STREAM, 0);
  assert(listenfd >= 0);
  ret = bind(listenfd, (struct sockaddr *)&address, sizeof(address));
  assert(ret != -1);

  ret = listen(listenfd, 5);
  assert(ret != -1);

  struct epoll_event events[MAX_EVENT_NUMBER];
  int epollfd = epoll_create(5);
  assert(epollfd != -1);

  /* 注意，监听 socket listenfd 上是不能注册 EPOLLONESHOT
   * 事件的，否则应用程序只能处理一个客户连接！因为后续的客户连接请求将不再触发
   * listenfd 上的 EPOLLIN 事件 */
  add_fd(epollfd, listenfd, 0);
  while (1) {
    int ret = epoll_wait(epollfd, events, MAX_EVENT_NUMBER, -1);
    if (ret < 0) {
      printf("epoll failure\n");
      break;
    }

    for (int i = 0; i < ret; ++i) {
      int sockfd = events[i].data.fd;
      if (sockfd == listenfd) {
        struct sockaddr_in client_address;
        socklen_t client_addrlength = sizeof(client_address);
        int connfd = accept(listenfd, (struct sockaddr *)&client_address,
                            &client_addrlength);
        /* 对每个非监听文件描述符都注册 EPOLLONESHOT 事件 */
        add_fd(epollfd, connfd, 1);
      } else if (events[i].events & EPOLLIN) {
        pthread_t thread;
        struct fds fds_for_new_worker;
        fds_for_new_worker.epollfd = epollfd;
        fds_for_new_worker.sockfd = sockfd;
        /* 新启动一个工作线程为 sockfd 服务 */
        pthread_create(&thread, NULL, worker, (void *)&fds_for_new_worker);
      } else {
        printf("something else happend\n");
      }
    }
  }

  close(listenfd);
  return 0;
}